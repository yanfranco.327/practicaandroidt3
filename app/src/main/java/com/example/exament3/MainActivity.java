package com.example.exament3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipDescription;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button; //Alt + enter : Para importar
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);         //Metodo: Father
        setContentView(R.layout.activity_main);     //Sirve: ingresar Layout


        Button btn = findViewById(R.id.btnClick);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("MAIN_APP", "hice clic en el boton");
                EditText edtName = findViewById(R.id.edtName);
                String texto = edtName.getText().toString();
                Log.i("MAIN_APP", "Texto es: " + texto);

                // llamar a otro actividad
                //Context context = getApplicationContext();
                // Context context = MainActivity.this;
                //Context context = this; // esto funciona fuera de la interface
/*
                if (!texto.trim().equals("")) {
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("NAME_EXTRA", texto);
                    startActivity(intent);                               //Sobrecarga :int ,floa,array ,etc.(Diccionario :Variable,Valorpara la variable)
                } else {
                    Log.i("MAIN_APP", "Inserte un nombre");
                    Toast.makeText(MainActivity.this, " ¡ Inserte un nombre !", Toast.LENGTH_LONG).show();
                }

                  Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:"
                        + "xyz@abc.com"
                        + "?subject=" + "Feedback" + "&body=" + "");
                intent.setData(data);
                startActivity(intent);

                */
                Uri uri = Uri.parse("smsto:960646522");
                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.putExtra("sms_body", "The SMS text prueba");
                startActivity(intent);

            }
        });



    }
}
