package com.example.exament3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MyListaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lista);
    }


    public List<String> GetPalabras() {
        List<String> palabras = new ArrayList<>();
        palabras.add("palabra 1");
        palabras.add("palabra 2");
        palabras.add("palabra 3");
        palabras.add("palabra 4");
        palabras.add("palabra 5");
        palabras.add("palabra 6");

        return palabras ;   
    }


}